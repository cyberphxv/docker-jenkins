FROM ruby:2.6.3-stretch

RUN apt-get update -qq && apt-get install -y netcat nodejs postgresql-client

ENV APP_HOME /rails

WORKDIR $APP_HOME

